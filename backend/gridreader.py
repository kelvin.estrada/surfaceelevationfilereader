"""
    This class receives an input grid file from the ADCIRC software and outputs the desired values for
    a given element number or nodal point of the grid.
"""
__author__ = "Kelvin O. Estrada Soto"
__email__ = "kelvin.estrada@upr.edu"
__date__ = "2022/06/08"


class GridReader:
    file = ""

    def __init__(self, filename):
        self.file = filename

    def read_element_and_node_numbers(self):
        """
        Reads input file and returns the total number of elements and nodal points in the grid file.
        :return: list
        """
        with open(self.file) as file:
            # skip first line
            file.readline()
            firstline = file.readline()
        file.close()
        tempstring = firstline.split()
        print(tempstring)
        res = []
        for s in tempstring:
            try:
                float(s)
                res.append(float(s))
            except ValueError:
                continue
        return res

    def read_grid_file_nodes(self):
        """
        Takes in an ADCIRC grid file and returns a list of the values of the nodal points.
        :return: values:
        """
        values = self.read_element_and_node_numbers()
        nodes = []
        with open(self.file) as file:
            # Skip first 2 lines of the file.
            for i in range(0, 2):
                file.readline()
            # Read all nodal points values.
            nodenumber = int(values[1])
            for i in range(2, nodenumber + 2):
                nodes.append(file.readline().split())
        file.close()
        print(nodes)
        """Retrieve all float values, ignore values that cannot be cast to float."""
        values = []
        for points in nodes:
            for value in points:
                try:
                    float(value)
                    values.append(float(value))
                except ValueError:
                    continue
        print(values)
        grouped = {}
        for i in range(0, len(values), 4):
            # Example: [1, -150.04586998, 61.21427000, 20.44440270]
            # is turned into {1: [-150.04586998, 61.21427000, 20.44440]}
            grouped[values[i]] = values[i + 1 : i + 4]
        print(grouped)
        return grouped

    def read_grid_file_elements(self):
        """
        Takes in an ADCIRC grid file and returns a list of the values of the element numbers.
        :return: values:
        """
        elms = []  # list to store node's (x,y,z) coordinate
        values = self.read_element_and_node_numbers()
        elementnumber = int(values[0])
        nodenumber = int(values[1])
        with open(self.file) as file:
            # Skip first nodenumber lines of the file.
            for i in range(0, nodenumber + 2):
                file.readline()
            # Read all nodal points values.
            for i in range(nodenumber + 1 + 1, nodenumber + elementnumber + 2):
                elms.append(file.readline().split(" "))
        file.close()

        """Retrieve all float values, ignore values that cannot be cast to float."""
        values = []
        for points in elms:
            for value in points:
                try:
                    float(value)
                    values.append(float(value))
                except ValueError:
                    continue
        grouped = {}
        for i in range(0, len(values), 5):
            grouped[values[i]] = values[i + 2 : i + 5]
        return grouped

    def get_element_connectivity(self, elementvalue):
        """
        Takes in the element number and the filename and returns the nodal connectivity for
        the specified element number.
        :param elementvalue:
        :return: connectivity: (list)
        """
        groups = self.read_grid_file_elements()
        connectivity = groups[elementvalue]
        if connectivity:
            return connectivity
        else:
            raise ValueError("Element number not found in grid file.")

    def get_nodal_point_coordinates(self, nodalpointvalue):
        """
        Takes in the nodal point value and the file name and returns the coordinates that belong to the
        given nodal point.
        :param nodalpointvalue:
        :return: coordinates: (list)
        """
        nodalpoints = self.read_grid_file_nodes()  # get all nodal points in a list
        coordinates = []
        coordinates = nodalpoints[nodalpointvalue]
        if coordinates:
            return coordinates
        else:
            raise ValueError("Node value not found in grid file.")
