__author__ = "Kelvin O. Estrada Soto"
__contact__ = "kelvin.estrada@upr.edu"
__date__ = "2022/07/12"


class Reader:
    """
        STWAVE Reader class.
        This class instantiates a reader object capable of handling STWAVE output files.
        Constructor takes in the file path of the file to be read.
    """

    def __init__(self, filename):
        """
        Class constructor.

        :param filename: File path to be read
        """
        self.file = filename
        self.dd = {}
        self.ds = {}
        self.snaps = {}
        self.data = []
        self.frequencies = []
        self.windspeed = []
        self.windir = []
        self.peakFreqs = []
        self.dadd = []
        self.xcoords = []
        self.ycoords = []

    def get_dd(self):
        """
        Getter for data dims namelist.

        :return: data dims namelist
        """
        return self.dd

    def get_ds(self):
        """
        Getter for data sets namelist.

        :return: data sets namelist
        """
        return self.ds

    def get_snaps(self):
        """
        Getter for records.

        :return: records
        """
        return self.snaps

    def get_data(self):
        """
        Getter for data values.

        :return: data values
        """
        return self.data

    def get_ni(self):
        """
        Getter for grid cell values in i-direction.

        :return: grid cells in i-direction
        """
        return int(self.dd['NI'])

    def get_nj(self):
        """
        Getter for grid cell values in the j-direction.

        :return: grid cells in the j-direction
        """
        return int(self.dd['NJ'])

    def get_recsnum(self):
        """
        Getter for number of records.

        :return: number of records
        """
        return int(self.dd['NumRecs'])

    def get_np(self):
        """
        Getter for number of points per record.

        :return: number of points
        """
        return self.get_nj() * self.get_ni()

    def get_records(self, record):
        """
        This function takes in a record number addressing the record the user wants to see.

        :param record:

        :return: returns all data for each record
        """
        if record == 0:
            return self.get_data()
        np = self.get_ni() * self.get_nj()
        end = np * record
        start = end - np
        return self.get_data()[start:end]

    def read_file(self):
        """
        Reads STWAVE Spatial Files data.
        :return:
        """

        # Read FORTRAN namelists
        foundAmper = False
        foundSlash = False

        with open(self.file, 'r') as f:
            # Read data dims --------------------------------------------------------------------------------
            while not foundAmper:
                line = f.readline()
                if len(line) == 0:
                    raise Exception("File cannot be read, it may be corrupt.")
                tmp = line[0]
                if tmp == '&':
                    foundAmper = True
                    # save data dims values ----------------------------------------------------------------
                    while not foundSlash:
                        line = f.readline()
                        line = line.replace(" ", "")
                        line = line.replace(",", "")
                        line = line.replace('\n', "")
                        if line[0] == '/':
                            foundSlash = True
                            break
                        line = line.split('=')
                        key = line[0]
                        value = line[1]
                        self.dd[key] = value
            # Read data sets --------------------------------------------------------------------------------
            foundAmper = False
            foundSlash = False
            while not foundAmper:
                line = f.readline()
                tmp = line[0]
                if tmp == '&':
                    foundAmper = True
                    # save data sets values ----------------------------------------------------------------
                    while not foundSlash:
                        line = f.readline()
                        line = line.replace(" ", "")
                        line = line.replace(",", "")
                        line = line.replace('\n', "")
                        if line[0] == '/':
                            foundSlash = True
                            break
                        line = line.split('=')
                        key = line[0]
                        value = line[1]
                        self.ds[key] = value
            # Read data ------------------------------------------------------------------------------------
            n = self.get_ni() * self.get_nj()
            recs = self.get_recsnum()
            for i in range(1, recs + 1):
                # clean record name and value
                line = f.readline().replace(" ", "").replace("\n", "")
                key = line[0:3] + str(i)
                value = line[3::]
                # add record name as key and record data as value
                self.snaps[key] = value
                for j in range(0, n):
                    # parse data line
                    line = f.readline().split()
                    # cast data line into float type
                    temp = []
                    for val in line:
                        temp.append(float(val))
                    # add data to array
                    self.data.append(temp)

    def read_file_spectral(self):
        """
        Reads STWAVE Spectral Files data.
        :return:
        """
        foundAmper = False
        foundSlash = False

        with open(self.file, 'r') as f:
            # Read data dims ------------------------------
            while not foundAmper:
                line = f.readline()
                tmp = line[0]
                if tmp == '&':
                    foundAmper = True
                    while not foundSlash:
                        line = f.readline()
                        line = line.replace(" ", "")
                        line = line.replace(",", "")
                        line = line.replace('\n', "")
                        if line[0] == '/':
                            foundSlash = True
                            break
                        line = line.split('=')
                        key = line[0]
                        value = line[1]
                        self.dd[key] = value
            f.readline()
            # READ FREQUENCIES ----------------------
            while 1:
                line = f.readline()
                tmp = line[0]
                if tmp == '#': break
                line = line.split()
                for val in line:
                    self.frequencies.append(float(val))
            # READ DATA
            numrecs = int(self.dd.get('numrecs'))
            numfreq = len(self.frequencies)
            for i in range(0, numrecs):
                line = f.readline()
                line = line.split()
                self.snaps['IDDS' + line[0]] = line[1::]
                for j in range(0, numfreq):
                    line = f.readline()
                    line = line.split()
                    # convert data to floats
                    for k in range(0, len(line)):
                        tmp = line[k]
                        line[k] = float(tmp)
                    self.data.append(line)
