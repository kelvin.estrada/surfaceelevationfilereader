class ElevationReader:

    def __init__(self, filename):
        self.file = filename
        self.elevationvalues = []
        self.decdaysandtimesteps = []
        self.timesteps = []

    def get_surface_elevations_every_node(self):
        """
        Get surface elevation values.
        :return: surface elevation values without node number.
        """
        if len(self.elevationvalues) == 0:
            raise Exception("File has not been read.")
        eleval = []
        for val in range(0, len(self.elevationvalues)):
            eleval.append(self.elevationvalues[val][1])
        return eleval

    def get_decdays_and_timesteps(self):
        """
        Get decimal days and timesteps.
        :return: decimals and timesteps list.
        """
        if len(self.decdaysandtimesteps) == 0:
            raise Exception("File has not been read.")
        return self.decdaysandtimesteps

    def get_timesteps_of_every_record(self):
        """
        Get timesteps of every record.
        :return: timesteps.
        """
        if len(self.timesteps) == 0:
            raise Exception("File has not been read.")
        return self.timesteps

    def get_all_elevations_for_node(self, node):
        """
        Get all surface elevation for a given node number accross all records.
        :param node:
        :return: list of surface elevation values
        """
        n = len(self.elevationvalues)
        startingpos = node - 1  # 0 indexed
        results = []
        numofnodes = self.read_num_of_nodes()
        numofrecords = self.read_num_of_records()
        elevations = self.get_surface_elevations_every_node()
        for i in range(startingpos, numofnodes * numofrecords, numofnodes):
            results.append(elevations[i])
        return results

    def get_all_elevations_for_time(self, currtimestep):
        """
        Get all surface elevation values under the time step.
        :param currtimestep:
        :return:
        """
        firstimestep = self.read_time_step()
        position = currtimestep // firstimestep
        np = self.read_num_of_nodes()
        endofrecord = position * np
        startofrecord = endofrecord - np
        nodes = self.get_surface_elevations_every_node()
        return nodes[startofrecord:endofrecord]

    def read_num_of_nodes(self):
        """
        Read the current file's node number.
        :return: nodenumber.
        """
        with open(self.file, "r") as f:
            # skip first line of description
            f.readline()
            line = f.readline()  # store string line that contains node number
        f.close()  # free resources
        linelist = line.split()  # remove whitespaces
        nodenumber = linelist[1]
        return int(nodenumber)

    def read_num_of_records(self):
        """
        Read current file number of datasets.
        :return: datasets.
        """
        with open(self.file, "r") as f:
            f.readline()
            line = f.readline()
        f.close()
        linelist = line.split()
        datasets = linelist[0]
        return int(datasets)

    def read_time_step(self):
        """
        Read current file time step value.
        :return: timestep
        """
        with open(self.file, "r") as f:
            f.readline()
            f.readline()
            line = f.readline()
        f.close()
        linelist = line.split()
        timestep = linelist[1]
        return int(timestep)

    def read_file(self):
        # Reading number of records and number of nodes in this file.
        numberofrecords = self.read_num_of_records()
        numberofnodes = self.read_num_of_nodes()

        # Open file for reading.
        with open(self.file, "r") as f:
            f.readline()  # skip first line
            f.readline()  # skip second line
            for i in range(0, numberofrecords):
                try:
                    temp = f.readline().split()
                    self.decdaysandtimesteps.append([float(temp[0]), int(temp[1])])
                    self.timesteps.append(int(temp[1]))
                except IndexError:
                    print(
                        "Expected to read %d records, but read %d records.\n You have %d missing records."
                        % (numberofrecords, i, numberofrecords - i)
                    )
                    break
                for j in range(0, numberofnodes):
                    line = f.readline().split()  # parse text line
                    # insert values to list
                    tmp = []
                    for k in range(0, len(line)):
                        if k == 0:
                            tmp.append(int(line[k]))
                        else:
                            tmp.append(float(line[k]))
                    self.elevationvalues.append(tmp)
        f.close()
        return [self.elevationvalues, self.decdaysandtimesteps, self.timesteps]
