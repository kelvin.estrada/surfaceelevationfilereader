import sys


def compare_scalars(arr1, arr2):
    """
    Compares the surface elevation values of two lists by calculating the difference pointer to pointer.
    From the difference calculated, the max, min, mean, absmax, absmin, absmean are calculated and returned.

    :param arr1: Contains all surface elevation values across all time steps for file 1.
    :param arr2: Contains all surface elevation values across all time steps for file 2.
    :return: a list: [[max, min, mean] , [absmax, absmin, absmean]]

    TODO: Keep track where values occured. Node number and time record.
    """
    minimum = sys.float_info.max
    maximum = -sys.float_info.max

    absminimum = sys.float_info.max
    absmaximum = -sys.float_info.max
    temp = 0
    abstemp = 0

    # if len(arr1) != len(arr2):
    #     raise Exception("Arrays are not of equal size.")
    # if len(arr1) == 0 and len(arr2) == 0:
    #     raise Exception("Inputs are empty arrays.")

    for i in range(0, len(arr1)):
        difference = arr1[i] - arr2[i]
        absdifference = abs(difference)
        # Comparisons
        if absdifference < absminimum:
            absminimum = absdifference
        if absdifference > absmaximum:
            absmaximum = absdifference

        if difference < minimum:
            minimum = difference
        if difference > maximum:
            maximum = difference
        temp += difference
        abstemp += absdifference

    average = temp / len(arr1)
    absaverage = abstemp / len(arr1)
    return [[maximum, minimum, average], [absmaximum, absminimum, absaverage]]


def compare_vectors_components(vector1, vector2):
    """
    Calculates the difference vector. Then calculates the max, min, mean, absmax, absmin, and absmean
    of each difference component vector.
    :param vector1: Contains all vector values for file 1 ==> [[u1, u2, u3, ..., uN], [v1, v2, v3, v4, ..., vN]].
    :param vector2: Contains all vector values for file 1 ==> [[u1, u2, u3, ..., uN], [v1, v2, v3, v4, ..., vN]].
    :return:
    """
    u1 = vector1[0]
    u2 = vector2[0]
    v1 = vector1[1]
    v2 = vector2[1]

    # Perform scalar operation on each component
    ucomponentstats = compare_scalars(u1, u2)
    vcomponentstats = compare_scalars(v1, v2)

    # Return each component's comparison.
    return [ucomponentstats, vcomponentstats]


def get_elevation_from_dataset(elevation, number, nodenumber):
    """
    Return the desired dataset values.
    :param nodenumber: A node number.
    :param elevation: A list that contains all datasets.
    :param number: A number that must be a multiple of 63.
    :return:
    """
    start = number - nodenumber
    return elevation[start:number]
