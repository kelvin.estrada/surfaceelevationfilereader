__author__ = "Kelvin O. Estrada Soto"
__contact__ = "kelvin.estrada@upr.edu"
__date__ = "2022/07/15"

from backend.stwavereader import Reader
import pathlib

CURRENT_DIR = pathlib.Path(__file__).parent
DEFAULT_DIR = CURRENT_DIR / "testresources/stwavefiles/spectralfiles"

"""
    These tests are designed to validate the STWAVE Reader class for reading STWAVE Spectral data files.
"""


def test_spectral_dd():
    reader = Reader(DEFAULT_DIR / "SimpleGrid.eng")
    reader.read_file_spectral()
    assert reader.get_dd() == {'datatype': '0', 'numrecs': '3', 'numfreq': '30', 'numangle': '72', 'numpoints': '1',
                               'azimuth': '0.0', 'coord_sys': '"LOCAL"', 'spzone': '17'}


def test_spectral_frequencies():
    reader = Reader(DEFAULT_DIR / "SimpleGrid.eng")
    reader.read_file_spectral()
    assert reader.frequencies == [0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1, 0.11, 0.12, 0.13, 0.14, 0.15, 0.16, 0.17,
                                  0.18, 0.19, 0.2, 0.21, 0.22, 0.23, 0.24, 0.25, 0.26, 0.27, 0.28, 0.29, 0.3, 0.31,
                                  0.32, 0.33]


def test_spectral_data():
    reader = Reader(DEFAULT_DIR / "SimpleGrid.eng")
    reader.read_file_spectral()
    data = reader.get_data()
    assert data[0] == [4.16540756e-020, 4.10236606e-020, 3.91799009e-020, 3.62604024e-020, 3.2478859e-020,
                       2.81034969e-020, 2.34304175e-020, 1.87549358e-020, 1.43441018e-020, 1.04135189e-020,
                       7.1109478e-021, 4.50840293e-021, 2.60337973e-021, 1.32877225e-021, 5.69986058e-022,
                       1.86914255e-022, 3.78737611e-023, 2.40348563e-024, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                       0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
                       0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 2.40348563e-024, 3.78737611e-023, 1.86914255e-022,
                       5.69986058e-022, 1.32877225e-021, 2.60337973e-021, 4.50840293e-021, 7.1109478e-021,
                       1.04135189e-020, 1.43441018e-020, 1.87549358e-020, 2.34304175e-020, 2.81034969e-020,
                       3.2478859e-020, 3.62604024e-020, 3.91799009e-020, 4.10236606e-020]

