import math
import sys

import pytest

from backend.stats import compare_scalars, compare_vectors_components

"""
    This script contain tests for the results comparison function developed for ADCIRC.
        - Tests the comparison function returns the expected values for scalars.
        - Tests the comparison function return the expected values for 'vector' inputs.
"""

testdata63 = [([1, 2, 3, 4], [5, 6, 7, 8], [[-4, -4, -4], [4, 4, 4]]),
              ([1.0, 2.0, 3.0, 4.0], [5.0, 6.0, 7.0, 8.0], [[-4.0, -4.0, -4.0], [4.0, 4.0, 4.0]]),
              ([1, 2.0, 3, 4.0], [5.0, 6.0, 7.0, 8], [[-4.0, -4.0, -4.0], [4.0, 4.0, 4.0]])]
testdatadiff63 = [([], [5.0, 6.0, 7.0, 8], []),
                  ([-1.0], [5.0, 7.0, 8], []),
                  ([1.0, -0.99999, 3.14159871], [1], [])]


# ------------------------------- Tests for *.63 files -----------------------------------------------------------
@pytest.mark.parametrize("a, b, expected", testdata63)
def test_get_stats_63(a, b, expected):
    results = compare_scalars(a, b)
    assert results == expected


@pytest.mark.parametrize("a, b, expected", testdatadiff63)
def test_get_stats_diff_lengths_63(a, b, expected):
    assert len(a) != len(b)


# ------------------------------- Tests for *.64 files -----------------------------------------------------------
# ----------u1-comp-------v1-comp---------u2-comp-------v2-comp----------------------------------------------------

def test_get_stats_64():
    results = compare_vectors_components([[-11, 22, 5, 0], [-5, 100, 50, -1]], [[11, -22, 7, 1], [30, 60, -50, 0]])
    expected = [[[44, -22, 4.75], [44, 1, 17.25]], [[100, -35, 26], [100, 1, 44]]]
    assert results == expected


def test_results_tolerance(a=1.1450492637E-006, b=1.1450381526E-006):
    diff_normalized = a - b
    print(diff_normalized)

