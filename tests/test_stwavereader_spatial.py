import pathlib
from backend.stwavereader import Reader
import pytest

CURRENT_DIR = pathlib.Path(__file__).parent
DEFAULT_DIR = CURRENT_DIR / "testresources/stwavefiles/spatialdata"

"""
    Date: 07/13/2022
    Author: Kelvin O. Estrada Soto
    
    These tests are dedicated to the STWAVE Reader class in charge of reading spatial datasets.
    Any spatial dataset output file from STWAVE that is read must be able to pass
    any of the tests.
    
    Tests:
        - Test if data read is consistent with the actual data.
        - Test if namelists (Data dims and Data sets) data is consistent with the actual file namelists.
        - Test if the desired record data was retrieved successfully from the file.
"""


def test_read_waveoutfile():
    reader = Reader(DEFAULT_DIR / "SimpleGrid.wave.out")
    reader.read_file()
    assert reader.data[0:6] == [[1.9298025612367098, 7.7553872755969895, -8.74689143411256083E-015],
                                [1.3123876060200941, 7.7551066921343752, 337.87723708127407],
                                [1.3117038493511219, 7.7548181990630818, 337.90662372331764],
                                [1.3110129434520303, 7.7545216357289410, 337.93617817879260],
                                [1.3103147720431647, 7.7542168395714066, 337.96590168958318],
                                [1.3096092138613735, 7.7539036129990349, 337.99579496434416]]


def test_read_waveoutfile_datadims():
    reader = Reader(DEFAULT_DIR / "SimpleGrid.wave.out")
    reader.read_file()
    assert reader.get_dd() == {'DataType': '0', 'NumRecs': '3', 'NumFlds': '3', 'NI': '71', 'NJ': '88',
                               'DX': '800.0000000000000000',
                               'DY': '800.0000000000000000', 'GridName': '"SimpleGrid.sim"'}


def test_read_waveoutfile_datasets():
    reader = Reader(DEFAULT_DIR / "SimpleGrid.wave.out")
    reader.read_file()
    assert reader.get_ds() == {'FldName(1)': '"WaveHeight"', 'FldName(2)': '"WavePeriod"',
                               'FldName(3)': '"WaveDirection"', 'FldUnits(1)': '"m"', 'FldUnits(2)': '"sec"',
                               'FldUnits(3)': '"deg"', 'RecInc': '1',
                               'RecUnits': '"n/a"', 'Reftime': '"1"'}


def test_read_waveoutfile_corrupt():
    reader = Reader(DEFAULT_DIR / "SimpleGrid.wave.corrupt")
    with pytest.raises(Exception):
        reader.read_file()


def test_get_records_waveoutfile(record=2):
    reader = Reader(DEFAULT_DIR / "SimpleGrid.wave.out")
    reader.read_file()
    data = reader.get_records(record)
    assert len(data) == reader.get_ni() * reader.get_nj()


def test_read_tpoutfile_data():
    reader = Reader(DEFAULT_DIR / "SimpleGrid.Tp.out")
    reader.read_file()
    assert reader.data[0:5] == [[10.000000000000000],
                                [10.000000000000000],
                                [10.000000000000000],
                                [10.000000000000000],
                                [10.000000000000000]]


def test_read_tpoutfile_datadims():
    reader = Reader(DEFAULT_DIR / "SimpleGrid.Tp.out")
    reader.read_file()
    assert reader.get_dd() == {'DataType': '0', 'NumRecs': '3', 'NumFlds': '1', 'NI': '71', 'NJ': '88',
                               'DX': '800.0000000000000000',
                               'DY': '800.0000000000000000', 'GridName': '"SimpleGrid.sim"'}


def test_read_depfile_data():
    reader = Reader(DEFAULT_DIR / "SimpleGrid.dep")
    reader.read_file()
    data = reader.get_data()
    assert data[0:5] == [[9.9750003814697], [9.9250001907349], [9.875], [9.8249998092651], [9.7749996185303]]


def test_read_depfile_records():
    reader = Reader(DEFAULT_DIR / "SimpleGrid.dep")
    reader.read_file()
    data = reader.get_records(1)
    np = reader.get_np()
    assert len(data) == np


def test_read_breakfile_data():
    reader = Reader(DEFAULT_DIR / "SimpleGrid.break.out")
    reader.read_file()
    data = reader.get_data()
    assert data[0:4] == [[0], [0], [0], [0]]
